<?php

get_header();

?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );
		}
	}

	?>
  <div class="pagination-holder">
    <?php
      previous_posts_link( 'Older Books' );
      next_posts_link( 'Newer Books' );
    ?>
  </div>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>

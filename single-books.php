<?php
get_header();

$terms = get_the_terms($post, 'genre');
?>

<main id="site-content" role="main" class="book-single-content">

  <h1><?php the_title() ?></h1>

  <?php echo  get_the_post_thumbnail(); ?>

  <h3><?php _e('Genre', 'custom_theme') ?>: 
    <?php foreach($terms as $key => $term): ?>
      <a href="<?php echo get_term_link($term->term_id, 'genre') ?>" title="<?php echo $term->name ?>"> 
        <?php echo $term->name ?>
      </a>
      <?php echo ( (count($terms) - 1 == $key ) ? '' : ', ' ); ?>


    <?php endforeach; ?>
  </h3>
  <?php the_content() ?>

  <date><?php _e('Book created:', 'custom_theme') ?> <?php echo get_the_date('d-m-Y') ?></date>


</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
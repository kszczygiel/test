<?php

//hooks
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
add_action( 'wp_enqueue_scripts', 'add_child_theme_custom_js' );
add_action( 'init', 'create_genre_taxonomy' );
add_action( 'init', 'books_post_type' );
add_action( 'pre_get_posts', 'genre_posts_limiter' );
add_action( 'after_switch_theme', 'flush_rewrite_rules' );

add_action('wp_ajax_get_latest_books', 'get_latest_books');
add_action('wp_ajax_nopriv_get_latest_books', 'get_latest_books');

add_action('wp_footer', 'add_ajax_url_to_globals');

add_shortcode( 'most_recent_book_title', 'get_most_recent_book_title' );
add_shortcode( 'most_recent_book_title', 'get_most_recent_book_title' );
add_shortcode( 'latest_books_genre', 'get_latest_books_by_genre' );

//functions
function get_latest_books($atts){
    $params = array(
        'post_type' => 'books',
        'numberposts' => 20,
        'posts_per_page' => 20,
    );

    $books = get_posts($params);

    $response = array();

    foreach($books as $book){
        $response[] = array(
            'name' => $book->post_title,
            'date' => strtotime($book->post_date),
            'genre' => get_the_terms($book->ID, 'genre')[0]->name,
            'excerpt' => get_the_excerpt($book)
        );
    }

    echo json_encode($response); die();
}


function get_latest_books_by_genre($atts){

    if(!$atts['genre_id']){
        return false;
    }
    $params = array(
        'post_type' => 'books',
        'numberposts' => 5,
        'orderby' => 'title',
        'order' => 'ASC',
        'tax_query' => array(
            array(
            'taxonomy' => 'genre',
            'field' => 'term_id',
            'terms' => $atts['genre_id']
             )
          )
    );

    $books = get_posts($params);
    $response = '';

    foreach ($books as $book){
        $response .= '<p> <a href="'. get_the_permalink($book->ID) .'"> '. $book->post_title .' </a> </p>';
    }

    return $response;
}


function get_most_recent_book_title($atts){
    $params = [
        'post_type' => 'books',
        'numberposts' => 1,
        'orderby' => 'date',
        'order' => 'DESC'
    ];

    $book = get_posts($params)[0];

    return ($book ? $book->post_title : false );
}

function genre_posts_limiter($query){
    if( $query->is_main_query() && is_tax( 'genre' ) && ! is_admin() ) {
        $query->set( 'posts_per_page', '5' );
    }
}

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

function add_ajax_url_to_globals(){
    ?>
    <script type="text/javascript">
        var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
    <?php
}

function add_child_theme_custom_js() {
  wp_register_script( 'child-custom-js', get_stylesheet_directory_uri().'/assets/js/scripts.js', array( 'jquery' ), '1.0.0', true );
  wp_enqueue_script( 'child-custom-js' );
}

function create_genre_taxonomy()
{
    register_taxonomy(
        'genre',
        'genre',
        array(
            'label' => __('Genre', 'custom_theme'),
            'rewrite' => array('slug' => 'book-genre'),
            'hierarchical' => true,
        )
    );
}

function books_post_type()
{
    {
        $labels = array(
            'name' => _x('Books', 'custom_theme'),
            'singular_name' => _x('Book', 'custom_theme'),
            'menu_name' => __('Books', 'custom_theme'),
            'parent_item_colon' => __('Parent' , 'custom_theme'),
            'all_items' => __('All items', 'custom_theme'),
            'view_item' => __('View item', 'custom_theme'),
            'add_new_item' => __('Add new', 'custom_theme'),
            'add_new' => __('Add new', 'custom_theme'),
            'edit_item' => __('Edit', 'custom_theme'),
            'update_item' => __('Update' , 'custom_theme'),
            'search_items' => __('Search' , 'custom_theme'),
            'not_found' => __('Not found', 'custom_theme'),
            'not_found_in_trash' => __('Not found in trash', 'custom_theme'),
        );
        $args = array(
            'label' => __('books'),
            'description' => __('Books archive', 'custom_theme'),
            'labels' => $labels,
            'supports' => array('title', 'thumbnail',  'editor'),
            'taxonomies' => array('genre'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 5,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-carrot',
            'rewrite' => array('slug' => 'library'),
        );
        register_post_type('books', $args);
    }
}

?>